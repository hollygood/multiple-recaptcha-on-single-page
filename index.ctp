<?php 
	echo $this->Html->script('https://www.google.com/recaptcha/api.js'); 
	echo $this->Html->css(array('jasny/jasny-bootstrap.min'), array('inline' => false));
	echo $this->Html->script(array('jasny/jasny-bootstrap.min'), array('inline' => false));

	echo $this->Js->buffer("
    $('#profile-tab').on('click', function(){
    	if($('.g-recaptcha').is(':empty'))
    		grecaptcha.render('RecaptchaField-D', {'sitekey' : '6LcdHQsTAAAAAL9ipoGI4x1TdLVcx76b9qESes1c'});
    });
	");
?>

<div class="container">
	<div class="row">
		<div class="col-xs-24">

			<div class="page-header">
				<?php 
					echo '<h1>' . $title . '</h1>'; 
					echo '<h2>' . $subTitle . '</h2>'; 
				?>
			</div>

			<div class="jumbotron">
				<!--
				<div class="row">
          <div class="col-xs-12">
					  <h3 class="text-primary"><?php echo __('join the dedicated logistics professionals'); ?></h3>
						<p><?php echo __('We are always looking for dynamic people to join our growing team of professionals. Please contact our Human Resource department by completing the form below and uploading your resume. '); ?></p>
					  <h3 class="text-primary"><?php echo __('Drivers'); ?></h3>
						<p><?php echo __('We are always looking for <em>Best in class drivers</em> across Canada as we grow our operations.  Please contact our team by completing the form and uploading your resume. '); ?></p>
          </div>

          <div class="col-xs-12">
						<?php echo $this->Element('form/resume'); ?>
					</div>
      
				</div>-->
				<?php echo $this->Session->flash(); ?>

				<div class="row">
					<div class="col-xs-24">
				    <ul role="tablist" class="nav nav-tabs" id="myTabs">
				      <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#home">join the dedicated logistics professionals</a></li>
				      <li role="presentation" class=""><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#profile" aria-expanded="false">Drivers</a></li>
				    </ul>

				    <div class="tab-content" id="myTabContent">
				      <div aria-labelledby="home-tab" id="home" class="tab-pane fade active in" role="tabpanel">
				        <?php 
				        	echo '<p>' . __('We are always looking for dynamic people to join our growing team of professionals. Please contact our Human Resource department by completing the form below and uploading your resume. ') . '</p>'; 
				        	echo $this->Element('form/resume', array('category' => 'G'));
				        ?>
				      </div>
				      <div aria-labelledby="profile-tab" id="profile" class="tab-pane fade" role="tabpanel">
				        <?php 
				        	echo '<p>' . __('We are always looking for <em>Best in class drivers</em> across Canada as we grow our operations.  Please contact our team by completing the form and uploading your resume. ') . '</p>'; 
				        	echo $this->Element('form/resume', array('category' => 'D')); 
				        ?>
				      </div>
				    </div>
				  </div>
			   </div>

			</div><!-- jumbotron -->

		</div>
	</div>
</div>
