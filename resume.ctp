<?php 
	echo $this->Form->create('Resume', array('type' => 'file', 'id' => 'ResumeIndexForm' . $category)); 
	echo $this->Form->input('name', array('label' => false, 'placeholder' => 'Name:'));
	echo $this->Form->input('title', array('label' => false, 'placeholder' => 'Title:'));
	echo $this->Form->input('company', array('label' => false, 'placeholder' => 'Company:'));
	echo $this->Form->input('phone', array('label' => false, 'placeholder' => 'Phone Number:'));
	echo $this->Form->input('email', array('label' => false, 'placeholder' => 'Email Address:'));
	echo $this->Form->input('category', array('type' => 'hidden', 'value' => $category));
?>

<div class="fileinput fileinput-new input-group" data-provides="fileinput">
	<div class="form-control" data-trigger="fileinput">
		<i class="fa fa-file fileinput-exists"></i> <span class="fileinput-filename"></span>
	</div>

	<span class="input-group-addon btn btn-default btn-file">
		<span class="fileinput-new">Browse</span>
		<span class="fileinput-exists">Change</span>
		<input type="file" name="data[Resume][file]" placeholder="Upload Your Resume" id="resume">
	</span>
	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
</div>

<?php	
	echo '<div id="RecaptchaField-' . $category . '" class="g-recaptcha" data-sitekey="6LcdHQsTAAAAAL9ipoGI4x1TdLVcx76b9qESes1c"></div>';
	echo $this->Form->submit('Submit Form', array('class' => 'btn-primary pull-right')); 
 	echo $this->Form->end(); 
?>